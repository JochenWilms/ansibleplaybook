#######################################
### Install Cloudwatch Logs Service ###
#######################################
# Install packages
    apt-get update -y && apt-get upgrade -y
    apt-get install python -y

# Create vars and files
    cat <<EOF >>CloudWatchConfig.cfg
[general]
state_file = /var/awslogs/state/agent-state

[/var/log/syslog]
file = /var/log/syslog
log_group_name = ${global_name}-/var/log/syslog
log_stream_name = {instance_id}
datetime_format = %b %d %H:%M:%S

[/var/log/jenkins/jenkins.log]
file = /var/log/jenkins/jenkins.log
log_group_name = ${global_name}-/var/log/jenkins/jenkins.log
log_stream_name = {instance_id}
datetime_format = %b %d %H:%M:%S
EOF

# Create aws logs
    curl https://s3.amazonaws.com/aws-cloudwatch/downloads/latest/awslogs-agent-setup.py -O
    chmod +x ./awslogs-agent-setup.py
    ./awslogs-agent-setup.py -n -r ${region} -c CloudWatchConfig.cfg
    mv /CloudWatchConfig.cfg /aws-scripts-mon/CloudWatchConfig.cfg


##########################################
### Install Cloudwatch Metrics Service ###
##########################################
# Install packages
    apt-get update -y && apt-get upgrade -y
    apt-get install unzip libwww-perl libdatetime-perl -y

# Create aws metrics
    curl https://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.2.zip -O
    unzip CloudWatchMonitoringScripts-1.2.2.zip
    rm CloudWatchMonitoringScripts-1.2.2.zip

# Create crontab for metrics
    cat <<EOF >>/aws-scripts-mon/crontab.txt
*/1 * * * * /aws-scripts-mon/mon-put-instance-data.pl --mem-used-incl-cache-buff --mem-util --disk-space-util --disk-path=/var/lib/jenkins --from-cron
EOF
    (crontab -l; cat /aws-scripts-mon/crontab.txt) | crontab -



# ##############################
# ### Newer Cloudwatch agent ### (wouldnt work, service wouldnt start)
# ##############################
# # Install packages
#     apt-get update -y && apt-get upgrade -y
#     wget https://s3.amazonaws.com/amazoncloudwatch-agent/linux/amd64/latest/AmazonCloudWatchAgent.zip
#     unzip AmazonCloudWatchAgent.zip
#     ./install.sh
#     /opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -c file:configuration-file-path -s
